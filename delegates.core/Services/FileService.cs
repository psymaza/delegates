using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using delegates.core.Events.Args;

namespace delegates.core.Services
{
    public static class FileService
    {
        private static event EventHandler<FileArgs> FileFoundEvent;

        private static void OnFileFoundEvent(string e)
        {
            FileFoundEvent?.Invoke(null, new FileArgs(e));
        }

        public static void AddFileFoundEventHandler(EventHandler<FileArgs> handler)
        {
            FileFoundEvent += handler;
        }

        public static void RemoveFileFoundEventHandler(EventHandler<FileArgs> handler)
        {
            FileFoundEvent -= handler;
        }

        public static void IterateFilesInDirectory(string path, CancellationToken? token = null)
        {
            var enumerator = Directory.GetFiles(path).GetEnumerator();
            while (enumerator.MoveNext() || (!token?.IsCancellationRequested ?? true))
                OnFileFoundEvent(enumerator.Current as string);
        }
    }
}