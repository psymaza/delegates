﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using delegates.core.Extensions;
using delegates.core.Services;

namespace delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            FileService.AddFileFoundEventHandler((sender, eventArgs) => Console.WriteLine(eventArgs.FileName));
            var cts = new CancellationTokenSource(TimeSpan.FromSeconds(1));
            FileService.IterateFilesInDirectory(Directory.GetCurrentDirectory(), cts.Token);
        }
    }
}